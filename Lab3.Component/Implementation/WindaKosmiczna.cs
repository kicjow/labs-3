﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3;


namespace Lab3.Implementation
{
   public class WindaKosmiczna : IZasilenie, IWjezdzaj, IConnect
    {
       private IConnect laczenie;
       private IWjezdzaj wjezdzanie;
       private IZasilenie zasielnie;

       public string lacz()
        {
            return "łącze z bazą";
        }

       public string test1()
        {
            return "testuje metode 1";
        }

       public string wjedz()
        {
            return  "wjeżdzam!";

        }

       public string test2()
        {
            return "testuje metode 2"; 
        }

       public string Zasilaj()
        {
            return "zasilam!";

        }

       public string test3()
        {
            return "testuje metode 3";
        }

       public static object UkladSterujacy(object component)
       {
           return new WindaKosmiczna();
       }

    }

    
}
