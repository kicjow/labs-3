﻿using System;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IConnect);
        public static Type I2 = typeof(IZasilenie);
        public static Type I3 = typeof(IWjezdzaj);

        public static Type Component = typeof(WindaKosmiczna);

        public static GetInstance GetInstanceOfI1 = new GetInstance(WindaKosmiczna.UkladSterujacy);
        public static GetInstance GetInstanceOfI2 = new GetInstance(WindaKosmiczna.UkladSterujacy);
        public static GetInstance GetInstanceOfI3 = new GetInstance(WindaKosmiczna.UkladSterujacy);
        
        #endregion

        #region P2

        public static Type Mixin = typeof(ClassaMixinFor);
        public static Type MixinFor = typeof(IConnect); // TODO

        #endregion
    }
}
